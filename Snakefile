from snakemake.utils import validate
import json

validate(config, "schema/config.schema.json")
print(json.dumps(config, indent=4))


def suffix_strip(s, suf):
    if(s.endswith(suf)):
        return s[:-1 * len(suf)]
    else:
        return suf


rule doall:
    input:
        config['target'] + ".tar.gz"


rule convert_VCF:
    input: 
        config['vcf']
    output:
        suffix_strip(suffix_strip(config['vcf'], '.gz'), '.vcf') + ".hmp.txt"
    params:
        memGb = int(config['mem_gb'] * 0.9)
    conda: 
        "envs/general.yml"
    resources:
        mem_mb = 1000 * config['mem_gb']
    shell:
        """
        run_pipeline.pl -vcf {input} -sortPositions -export {input} -exportType Hapmap -Xmx{params.memGb}G
        mv {input}.hmp.txt {output}
        """

rule run_GAPIT_GWAS:
    input:
       rules.convert_VCF.output[0],
       config['phenotypes']
    output:
        config['target']+".tar.gz"
    params:
        outdir=config['target'],
        model=config['gapit_opts']['model'],
        kinship_algorithm = config['gapit_opts']['kinship.algorithm'],
        kinship_cluster = config['gapit_opts']['kinship.cluster'],
        kinship_group = config['gapit_opts']['kinship.group'],
        snp_effect = config['gapit_opts']['SNP.effect'],
        pca_total = config['gapit_opts']['PCA.total'],
        snp_fraction = config['gapit_opts']['SNP.fraction']
    resources:
        mem_mb = config['mem_gb'] * 1000
    conda: 
        "envs/general.yml"
    script:
        "scripts/GAPIT_runner.R"


        


